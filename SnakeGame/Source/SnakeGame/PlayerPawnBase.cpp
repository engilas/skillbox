// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "FoodSpawner.h"
#include "Kismet/GameplayStatics.h"
#include "LevelManager.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorRotation(FRotator(-90, 0, 0));

	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass);

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "LevelManager", Actors);
	auto LevelManager = Cast<ALevelManager>(Actors[0]);
	LevelManager->Snake = SnakeActor;

	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "FoodSpawner", Actors);
	auto FoodSpawner = Cast<AFoodSpawner>(Actors[0]);
	FoodSpawner->SpawnFood();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandleVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandleHorizontalInput);

}

void APlayerPawnBase::HandleVerticalInput(float value)
{
	if (!IsValid(SnakeActor)) 
		return;
	
	if (SnakeActor->LastMoveDirection != EMoveDirection::UP && SnakeActor->LastMoveDirection != EMoveDirection::DOWN)
	{
		if (value > 0)
		{
			SnakeActor->LastMoveDirection = EMoveDirection::UP;
		}
		else if (value < 0)
		{
			SnakeActor->LastMoveDirection = EMoveDirection::DOWN;
		}
	}

	bVerticalPressed = value != 0;
	SetPlayerSpeed();
}

void APlayerPawnBase::HandleHorizontalInput(float value)
{
	if (!IsValid(SnakeActor)) return;

	if (SnakeActor->LastMoveDirection != EMoveDirection::LEFT && SnakeActor->LastMoveDirection != EMoveDirection::RIGHT)
	{
		if (value > 0)
		{
			SnakeActor->LastMoveDirection = EMoveDirection::RIGHT;
		}
		else if (value < 0)
		{
			SnakeActor->LastMoveDirection = EMoveDirection::LEFT;
		}
	}

	bHorizontalPressed = value != 0;
	SetPlayerSpeed();
}

void APlayerPawnBase::SetPlayerSpeed()
{
	if (!bVerticalPressed && !bHorizontalPressed)
	{
		SpeedHold = 0;
	}
	else if (SpeedHold == 0)
	{
		SpeedHold = GetWorld()->GetTimeSeconds();
	}
	
	if ((bVerticalPressed || bHorizontalPressed) && GetWorld()->GetTimeSeconds() - SpeedHold > 0.5f)
	{
		SnakeActor->SetFastSpeed();
	}
	else
	{
		SnakeActor->SetSlowSpeed();
	}
}