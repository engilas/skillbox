// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodSpawner.h"

// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawner::SpawnFood()
{
	static const FMath Math;

	const float X = Math.RandRange(MinX, MaxX);
	const float Y = Math.RandRange(MinY, MaxY);
	AFood* Food = GetWorld()->SpawnActor<AFood>(FoodClass, FTransform(FVector(X, Y, 0)));
	
}