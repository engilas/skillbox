// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.0f;
	LastMoveDirection = EMoveDirection::DOWN;
	Speed = 0.5f;
	FastSpeed = 0.2f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(Speed);
	AddSnakeElement(5, true);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum, bool bVisibility)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		auto NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		NewSnakeElem->MeshComponent->SetVisibility(bVisibility);
		int32 index = SnakeElements.Add(NewSnakeElem);
		if (index == 0)
		{
			NewSnakeElem->SetFirstElement();
		}
	}
}

void ASnakeBase::Move()
{
	float MovementSpeed = ElementSize;
	FVector MoveVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMoveDirection::UP:
		MoveVector.X += MovementSpeed;
		break;
	case EMoveDirection::DOWN:
		MoveVector.X -= MovementSpeed;
		break;
	case EMoveDirection::LEFT:
		MoveVector.Y += MovementSpeed;
		break;
	case EMoveDirection::RIGHT:
		MoveVector.Y -= MovementSpeed;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		ASnakeElementBase* CurrentElement = SnakeElements[i];
		ASnakeElementBase* NextElement = SnakeElements[i - 1];
		FVector NextLocation = NextElement->GetActorLocation();
		CurrentElement->SetActorLocation(NextLocation);
		CurrentElement->MeshComponent->SetVisibility(true);
	}

	SnakeElements[0]->AddActorWorldOffset(MoveVector);
	SnakeElements[0]->ToggleCollision();
	SnakeElements[0]->MeshComponent->SetVisibility(true);
}

void ASnakeBase::SnakeElementOverlapped(ASnakeElementBase* SnakeElement, AActor* Other)
{
	if (!IsValid(SnakeElement))
		return;

	int32 elementIndex;
	SnakeElements.Find(SnakeElement, elementIndex);
	bool bIsFirst = elementIndex == 0;
	IInteractable* Interactable = Cast<IInteractable>(Other);
	if (Interactable != nullptr)
	{
		Interactable->Interact(this, bIsFirst);
	}
}

void ASnakeBase::SetFastSpeed()
{
	if (!bFastSpeed)
	{
		SetActorTickInterval(FastSpeed);
		bFastSpeed = true;
	}
}

void ASnakeBase::SetSlowSpeed()
{
	if (bFastSpeed)
	{
		SetActorTickInterval(Speed);
		bFastSpeed = false;
	}
}

