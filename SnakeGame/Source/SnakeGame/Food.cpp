// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "FoodSpawner.h"
#include "Kismet/GameplayStatics.h"
#include "LevelManager.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "LevelManager", Actors);
	LevelManager = Cast<ALevelManager>(Actors[0]);
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "FoodSpawner", Actors);
	FoodSpawner = Cast<AFoodSpawner>(Actors[0]);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsFirst)
{
	if (!bIsFirst)
		return;

	auto Snake = Cast<ASnakeBase>(Interactor);
	if (!IsValid(Snake))
		return;

	Snake->AddSnakeElement();
	Destroy();

	if (!LevelManager->OnFoodCollect())
	{
		// no transition to next level
		FoodSpawner->SpawnFood();
	}
}