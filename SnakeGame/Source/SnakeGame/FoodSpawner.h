// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraComponent.h"
#include "Food.h"
#include "FoodSpawner.generated.h"

UCLASS()
class SNAKEGAME_API AFoodSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFoodSpawner();

	UPROPERTY(EditDefaultsOnly)
		float MinX;
	UPROPERTY(EditDefaultsOnly)
		float MinY;
	UPROPERTY(EditDefaultsOnly)
		float MaxX;
	UPROPERTY(EditDefaultsOnly)
		float MaxY;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void SpawnFood();

private:
	float FloorZ;
};
