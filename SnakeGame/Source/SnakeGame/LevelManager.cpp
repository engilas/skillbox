// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelManager.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "SnakeGameInstance.h"

// Sets default values
ALevelManager::ALevelManager() : CurrentLevel(1), CollectCount(0)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALevelManager::BeginPlay()
{
	Super::BeginPlay();

	TransitionMap.Add(1, 10);
	TransitionMap.Add(2, 15);
	TransitionMap.Add(3, 20);

	CurrentLevel = Cast<USnakeGameInstance>(GetGameInstance())->CurrentLevel;
}

// Called every frame
void ALevelManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ALevelManager::OnFoodCollect()
{
	CollectCount++;
	int RequiredCollects = TransitionMap[CurrentLevel];
	if (CollectCount == RequiredCollects)
	{
		int NextLevel = CurrentLevel + 1;
		if (NextLevel > 3)
		{
			NextLevel = 1;
		}
		Cast<USnakeGameInstance>(GetGameInstance())->CurrentLevel = NextLevel;
		FName LevelName = FName(TEXT("Level" + FString::FromInt(NextLevel)));
		UGameplayStatics::OpenLevel(GetWorld(), LevelName);
		return true;
	}
	return false;
}