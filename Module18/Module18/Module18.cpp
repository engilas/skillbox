// Module18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>

template<class T>
class Stack
{
public:
	Stack(int size) : maxSize(size), size(0)
	{
		data = new T[maxSize];
	}
	
	~Stack()
	{
		delete[] data;
	}

	void Push(const T& value)
	{
		if (size < maxSize)
		{
			data[size++] = value;
		}
		else
		{
			throw std::exception("Stack is full");
		}
	}

	T& Pop()
	{
		if (size > 0)
		{
			return data[--size];
		}
		throw std::exception("Stack is empty");
	}
	
private:
	int maxSize;
	int size;
    T* data;
};

int main()
{
	Stack<int> stack(100);

	stack.Push(51);
	stack.Push(617);
	stack.Push(12);

	for(int i = 0; i < 3; i++)
	{
		std::cout << stack.Pop() << std::endl;
	}
    
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
