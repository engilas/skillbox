// Module17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Student
{
public:
	Student(std::string name, int age) : name(name), age(age)
	{
	}

	void Print()
	{
		std::cout << "Name: " << name << " Age: " << age << std::endl;
	}
	
private:
	std::string name;
	int age;
};

class Vector
{
public:
	Vector(double x, double y, double z) : x(x), y(y), z(z)
	{
	}
	
	void Show()
	{
		std::cout << x << " " << y << " " << z << std::endl;
	}

	double Length()
	{
		return sqrt(x * x + y * y + z * z);
	}
	
private:
	double x, y, z;
};

int main()
{
	Student student("John", 20);
	student.Print();

	Vector vector(5, 2, 4);
	vector.Show();
	std::cout << "Vector length: " << vector.Length() << std::endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
