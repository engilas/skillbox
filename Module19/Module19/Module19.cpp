// Module19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
	}

	virtual ~Animal()
	{
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow\n";
	}
};

class Pig : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Wee\n";
	}
};

int main()
{
	Animal* animals[3];

	animals[0] = new Dog;
	animals[1] = new Cat;
	animals[2] = new Pig;

	for (auto animal : animals)
	{
		animal->Voice();
	}

	for (auto animal : animals)
	{
		delete animal;
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
